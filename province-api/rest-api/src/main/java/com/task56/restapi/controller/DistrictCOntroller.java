package com.task56.restapi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56.restapi.model.CDistrict;
import com.task56.restapi.model.CWard;
import com.task56.restapi.service.DistrictService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DistrictCOntroller {
    @Autowired
    private DistrictService districtService;

    @GetMapping("/districts")
    public ResponseEntity<List<CDistrict>> getAllDistrict() {
        try {
            return new ResponseEntity<>(districtService.getAllDistrict(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/ward")
    public ResponseEntity<Set<CWard>> getWardByDistrictId(
            @RequestParam(value = "districtId") long districtId) {
        try {
            Set<CWard> ward = districtService.getWarByDistrictId(districtId);
            if (ward != null) {
                return new ResponseEntity<>(ward, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
