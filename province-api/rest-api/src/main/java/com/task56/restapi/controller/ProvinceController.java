package com.task56.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.service.internal.ProvidedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56.restapi.model.CDistrict;
import com.task56.restapi.model.CProvince;
import com.task56.restapi.respository.IProvinceRespository;
import com.task56.restapi.service.ProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ProvinceController {
    @Autowired
    private ProvinceService providedService;
    IProvinceRespository provinceRespository;

    @GetMapping("/provinces")
    public ResponseEntity<List<CProvince>> getAllProvince() {
        try {
            return new ResponseEntity<>(providedService.getAllProvince(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province5")
    public ResponseEntity<List<CProvince>> getFiveProvince(
            @RequestParam(value = "page", defaultValue = "1") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CProvince> list = new ArrayList<CProvince>();
            provinceRespository.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/district")
    public ResponseEntity<Set<CDistrict>> getDistrictByprovinceId(
            @RequestParam(value = "provinceId") long provinceId) {
        try {
            Set<CDistrict> district = providedService.getDistrictByProvinceId(provinceId);
            if (district != null) {
                return new ResponseEntity<>(district, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
