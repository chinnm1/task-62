package com.task56.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task56.restapi.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Long> {
    CDistrict findByDistrictId(long districtId);
}
