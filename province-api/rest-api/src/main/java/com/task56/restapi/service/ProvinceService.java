package com.task56.restapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.task56.restapi.model.CDistrict;
import com.task56.restapi.model.CProvince;
import com.task56.restapi.respository.IProvinceRespository;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRespository provinceRespository;

    public ArrayList<CProvince> getAllProvince() {
        ArrayList<CProvince> listProvince = new ArrayList<>();
        provinceRespository.findAll().forEach(listProvince::add);
        return listProvince;
    }
    


    public Set<CDistrict> getDistrictByProvinceId(long provinceId) {
        CProvince vProvince = provinceRespository.findByProvinceId(provinceId);
        if (vProvince != null) {
            return vProvince.getDistricts();
        } else {
            return null;
        }
    }

}
