package com.task56.restapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ward")
public class CWard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long wardId;

    @Column(name = "name_ward")
    private String nameWard;
    @Column(name = "prefix_ward")
    private String prefixWard;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "district_id")
    private CDistrict district;

    public CWard(long wardId, String nameWard, String prefixWard, CDistrict district) {
        this.wardId = wardId;
        this.nameWard = nameWard;
        this.prefixWard = prefixWard;
        this.district = district;
    }

    public CWard() {
    }

    public long getWardId() {
        return wardId;
    }

    public void setWardId(long wardId) {
        this.wardId = wardId;
    }

    public String getNameWard() {
        return nameWard;
    }

    public void setNameWard(String nameWard) {
        this.nameWard = nameWard;
    }

    public String getPrefixWard() {
        return prefixWard;
    }

    public void setPrefixWard(String prefixWard) {
        this.prefixWard = prefixWard;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

}
